const api = require('../lib/RedAlertApi');
const express = require('express');
const router = express.Router();

router.use((req, res, next) => {
	res.locals.current = 'announcements';
	next();
});

router.get('/', (req, res, next) => {
	api.getAnnouncements()
		.then(announcements => res.render('announcements', announcements))
		.catch(err => next(err));
});

router.get('/json', (req, res, next) => {
	api.getAnnouncements()
		.then(announcements => res.json(announcements))
		.catch(err => next(err));
});

router.get('/:id', (req, res, next) => {
	res.locals.current = 'announcement';

	if (!req.params.id)
		return res.sendStatus(404);

	api.getAnnouncement(req.params.id)
		.then(announcement => res.render('announcement', {announcement}))
		.catch(err => next(err));
});

module.exports = router;
