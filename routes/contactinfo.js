const api = require('../lib/RedAlertApi');
const router = require('express').Router();

router.use((req, res, next) => {
	res.locals.current = 'contactinfo';
	next();
});

router.get('/', (req, res, next) => {
	api.getContactInfo()
		.then(contactInfo => res.render('contactinfo', {contactInfo}))
		.catch(err => next(err));
});

module.exports = router;
