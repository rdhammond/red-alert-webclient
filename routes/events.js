const api = require('../lib/RedAlertApi');
const router = require('express').Router();

router.use((req, res, next) => {
	res.locals.current = 'events';
	next();
});

router.get('/', (req, res, next) => {
	api.getEvents()
		.then(events => res.render('events', events))
		.catch(err => next(err));
});

router.get('/json', (req, res, next) => {
	api.getEvents()
		.then(events => res.json(events))
		.catch(err => next(err));
});

router.get('/:id', (req, res, next) => {
	res.locals.current = 'event';

	if (!req.params.id)
		return res.sendStatus(404);
	
	api.getEvent(req.params.id)
		.then(ev => res.render('event', {event: ev}))
		.catch(err => next(err));
});

module.exports = router;
