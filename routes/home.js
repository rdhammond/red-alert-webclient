const router = require('express').Router();

router.get('/', (req, res) => res.redirect('/announcements'));

module.exports = router;
