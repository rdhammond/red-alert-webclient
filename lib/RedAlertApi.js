const axios = require('axios');
const config = require('../config');

const PUBLIC_APIKEY = process.env.PUBLIC_APIKEY;

const options = {
	baseURL: config.apiUrl,
	responseType: 'json'
};

function get(url) {
	return axios.get(url, options).then(x => Promise.resolve(x.data));
}

module.exports = {
	getAnnouncements: () => get(`/announcements?apikey=${PUBLIC_APIKEY}`),
	getAnnouncement: (id) => get(`/announcements/${id}?apikey=${PUBLIC_APIKEY}`),
	getEvents: () => get(`/events?apikey=${PUBLIC_APIKEY}`),
	getEvent: (id) => get(`/events/${id}?apikey=${PUBLIC_APIKEY}`),
	getContactInfo: () => get(`/contactinfo?apikey=${PUBLIC_APIKEY}`)
};
