const sass = require('node-sass');

module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// Copy web assets from bower_components to more convenient directories.
		copy: {
			main: {
				files: [
					// Vendor scripts.
					{
						expand: true,
						cwd: 'bower_components/bootstrap/dist/js/',
						src: ['bootstrap.min.js', 'bootstrap.min.js.map'],
						dest: 'public/scripts/',
						flatten: true
					},
					{
						expand: true,
						cwd: 'bower_components/jquery/dist/',
						src: ['jquery.min.js', 'jquery.min.map'],
						dest: 'public/scripts/',
						flatten: true
					},
					{
						expand: true,
						cwd: 'bower_components/popper.js/dist/umd/',
						src: ['popper.min.js', 'popper.min.js.map'],
						dest: 'public/scripts/',
						flatten: true
					},
					{
						expand: true,
						cwd: 'bower_components/ionicons/docs/css/',
						src: ['ionicons.min.css'],
						dest: 'public/css/',
						flatten: true
					},
					{
						expand: true,
						cwd: 'bower_components/moment/min/',
						src: ['moment.min.js'],
						dest: 'public/scripts/',
						flatten: true
					},
					{
						expand: true,
						flatten: true,
						cwd: 'bower_components/jquery-load-template/dist/',
						src: ['jquery.loadTemplate.min.js'],
						dest: 'public/scripts/'
					},

					// Images
					{
						expand: true,
						flatten: true,
						cwd: 'client/images/',
						src: ['**/*'],
						dest: 'public/images/'
					},

					// Fonts
					{
						expand: true,
						flatten: true,
						cwd: 'bower_components/ionicons/docs/fonts/',
						src: ['**/*'],
						dest: 'public/fonts/'
					}
				]
			},
		},

		// Compile SASS files into minified CSS.
		sass: {
			dist: {
				options: {
					implementation: sass,
					sourceMap: true,
					includePaths: [
						'bower_components/bootstrap/scss/'
					]
				},
				files: {
					'public/css/app.min.css': 'client/scss/app.scss'
				}
			}
		},

		// Minify custom JS
		uglify: {
			dist: {
				files: [
					{
						expand: true,
						cwd: 'client/scripts',
						src: '**/*.js',
						dest: 'public/scripts',
						rename: function(dst, src) {
							return dst + '/' + src.replace('.js', '.min.js');
						}
					}
				]
			}
		},

		// Watch for changes and recompile
		watch: {
			scripts: {
				files: ['client/scripts/**/*.js'],
				tasks: ['uglify']
			},
			sass: {
				files: ['client/scss/**/*.scss'],
				tasks: ['sass']
			},
			images: {
				files: ['client/images/**/*'],
				tasks: ['copy']
			},
			components: {
				files: ['bower_components/**/*.js', 'bower_components/**/*css'],
				tasks: ['copy']
			}
		}
	});

	// Load externally defined tasks. 
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Establish tasks we can run from the terminal.
	grunt.registerTask('build', ['sass', 'copy', 'uglify']);
	grunt.registerTask('default', ['build', 'watch']);
}
