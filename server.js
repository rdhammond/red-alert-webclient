const config = require('./config');

const moment = require('moment');
const favicon = require('serve-favicon');
const express = require('express');
let app = express();
app.set('view engine', 'pug');
app.use(favicon('favicon.png'));
app.use(express.static('public'));

app.use((req, res, next) => {
	res.locals.moment = moment;
	res.locals.apiUrl = config.apiUrl;
	res.locals.branding = config.branding;
	res.locals.headerColor = config.headerColor;
	next();
});

app.use('/', require('./routes/home'));
app.use('/announcements', require('./routes/announcements'));
app.use('/events', require('./routes/events'));
app.use('/contactinfo', require('./routes/contactinfo'));

let port = process.env.PORT || config.port;
app.listen(port, () => console.log(`${config.branding} web client listening on port ${port}`));
