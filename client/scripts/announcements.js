(function($) {
	'use strict';

	var TIMEOUT_MS = 15*60*1000;

	var timer;

	function updateAnnouncements() {
		$.get('/announcements/json', function(announcements) {
			var $announcements = $('.announcements');
			var announcementsChanged = $announcements.children().length !== announcements.length;
			$announcements.empty();

			for (var i=0; i<announcements.length; i++) {
				var announcement = announcements[i];
				var eventTime = moment(announcement.eventTime);
				announcement.date = eventTime.format('M/D/YYYY');
				announcement.time = eventTime.format('h:mm A');
				announcement.body = (announcement.body.length <= 100)
					? announcement.body
					: announcement.body.substr(0,100) + '...';

				var $wrapper = $('<div></div>');
				$wrapper.loadTemplate($('#announcementTemplate'), announcement);
				var $announcement = $wrapper.children('a').first();
				$announcement.attr('data-id', announcement._id);
				$announcement.attr('href', '/announcements/' + announcement._id);
				$announcement.detach();
				$announcement.appendTo($announcements);
				$announcement.appendTo($announcements);
			}

			if (announcementsChanged)
				window.sendNotification('Red Alert', 'New Announcements', '/announcements');

			timer = window.setTimeout(updateAnnouncements, TIMEOUT_MS);
		});
	}

	$(function() {
		window.setTimeout(updateAnnouncements, TIMEOUT_MS);
	});
})(jQuery);
