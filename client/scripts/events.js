(function($) {
	'use strict';

	var TIMEOUT_MS = 15*60*1000;

	var timer;

	function updateEvents() {
		$.get('/events/json', function(events) {
			var $events = $('.events');
			var eventsChanged = $events.children().length !== events.length;
			$events.empty();

			for (var i=0; i<events.length; i++) {
				var ev = events[i];
				var evTime = moment.utc(ev.eventTime);
				evTime.local();
				ev.datetime = evTime.format('M/D/YYYY') + ' @ ' + evTime.format('h:mm A');

				var $wrapper = $('<div></div>');
				$wrapper.loadTemplate($('#eventTemplate'), ev);
				var $ev = $wrapper.children('a').first();
				$ev.attr('data-id', ev._id);
				$ev.attr('href', '/events/' + ev._id);
				$ev.detach();
				$ev.appendTo($events);
			}

			if (eventsChanged)
				window.sendNotification('Red Alert', 'New events available!', '/events');

			timer = window.setTimeout(updateEvents, TIMEOUT_MS);
		});
	}

	// Heroku is hosted in a completely different timezone and any timestamps
	// generated upstream will not match the user's. We have to parse them
	// locally.
	function initializeDates() {
		$('.event .timestamp').each(function() {
			var $this = $(this);
			var datetime = moment.utc($this.data('datetime'));
			datetime.local();
			$this.text(datetime.format('M/D/YYYY') + ' @ ' + datetime.format('h:mm A'));
		});
	}

	$(function() {
		initializeDates();
		window.setTimeout(updateEvents, TIMEOUT_MS);
	});
})(jQuery);
