(function() {
	'use strict';

	// ** HACK: Google insists on loading its own fonts for Maps API, which
	// clobbers the ones Bootstrap already loaded. This should work around it.
	function suppressMapFontsLoad() {
		var head = document.getElementsByTagName('head')[0];
		var oldInsertBefore = head.insertBefore;

		head.insertBefore = function(newElement, referenceElement) {
			if (newElement.href && newElement.href.indexOf('//fonts.googleapis.com/css?family=Roboto') !== -1)
				return;

			return oldInsertBefore.call(head, newElement, referenceElement);
		};
	}

	window.initMap = function() {
		var contents = document.getElementById('mapContents');
		var coords = {
			lat: parseFloat(contents.dataset.lat, 10),
			lng: parseFloat(contents.dataset.lon, 10)
		};
		var map = new google.maps.Map(contents, {zoom: 14, center: coords});
		var marker = new google.maps.Marker({position: coords, map: map});
	};

	suppressMapFontsLoad();
})();
