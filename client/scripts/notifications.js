(function($, window) {
	'use strict';

	var Notification = window.Notification;

	function getPermissionForNotification() {
		if (Notification.permission === 'denied')
			return false;
		if (Notification.permission === 'granted')
			return true;
		
		Notification.requestPermission();
	}

	function sendNotification(title, body, url) {
		if (!Notification || Notification.permission !== 'granted')
			return;

		var notification = new Notification(title, {
			body: body
		});
		notification.onclick = function() {
			if (url)
				window.location = url;
		};
		setTimeout(notification.close.bind(notification), 5000);
	}

	$(function() {
		if (Notification && Notification.permission === 'default')
			Notification.requestPermission();

		window.sendNotification = sendNotification.bind(Notification);
	});
})(jQuery, window);
